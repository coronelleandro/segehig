<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" href="css/index.css">
    <link rel="stylesheet" type="text/css" href="css/contact.css">
    <link rel="stylesheet" type="text/css" href="css/services.css">
    <link rel="stylesheet" type="text/css" href="css/about-us.css">

    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>

    <title>Hello, world!</title>
  </head>
  <body>
    
  	<header class="">
      <nav class="nav"> 
        <div class="logo">
          <a href="">Logo</a>
        </div>
        <div class="div-menu">
          <a href="">Inicio</a>
          <a href="">Servicio</a>
          <a href="">Quienes Somos</a>
          <a href="">Contacto</a>
        </div>
      </nav>
    </header>

    <section class="section-index">
      <div class="div-index">
        <?php 
          require_once('views/services.php');
          require_once('views/about-us.php'); 
          require_once('views/contact.php');
        ?>
      </div>
    </section>

    <footer>
      <div>
        <p>Copyright © 2020 Todos los derechos reservados</p>
      </div>
    </footer>
  </body>
</html>
